FROM ubuntu:focal
ENV TZ="Europe/Berlin"
RUN apt update
RUN DEBIAN_FRONTEND="noninteractive" apt -y install tzdata
RUN apt -y install git clang-format gcc g++ cmake ccache curl unzip libusb-1.0-0-dev libevdev-dev libboost1.67-all-dev libcrypto++-dev rapidjson-dev libdouble-conversion-dev libpcre2-dev mesa-common-dev libpng16-16 libharfbuzz-bin ssh doxygen graphviz software-properties-common

